import { initializeApp } from "firebase/app";
import  'firebase/compat/firestore';

import { getFirestore } from 'firebase/firestore'


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyACsqt1yALDNKBOxznMBQcZi3DQFAfm2_I",
  authDomain: "house-app-4b376.firebaseapp.com",
  projectId: "house-app-4b376",
  storageBucket: "house-app-4b376.appspot.com",
  messagingSenderId: "617796987698",
  appId: "1:617796987698:web:665c10d7484e65d8ecc52b"
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const db = getFirestore()